﻿using dentistservice.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace dentistservice
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Teeth> Teeths { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<DentalChart> DentalCharts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Projects\GitRepositories\dentistservice\dentistservice\dentistservice");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();

            var connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(x => x.Login)
                .IsUnique();
        }
    }
}
