﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace dentistservice.Migrations
{
    public partial class AddNewModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Visits",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Teeths",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JawType = table.Column<int>(nullable: false),
                    JawSide = table.Column<int>(nullable: false),
                    ToothNo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teeths", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    IsBlocked = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    OrganizationID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_Organizations_OrganizationID",
                        column: x => x.OrganizationID,
                        principalTable: "Organizations",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DentalCharts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Info = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    TeethID = table.Column<int>(nullable: false),
                    VisitID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DentalCharts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DentalCharts_Teeths_TeethID",
                        column: x => x.TeethID,
                        principalTable: "Teeths",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DentalCharts_Visits_VisitID",
                        column: x => x.VisitID,
                        principalTable: "Visits",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Visits_UserID",
                table: "Visits",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_DentalCharts_TeethID",
                table: "DentalCharts",
                column: "TeethID");

            migrationBuilder.CreateIndex(
                name: "IX_DentalCharts_VisitID",
                table: "DentalCharts",
                column: "VisitID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_OrganizationID",
                table: "Users",
                column: "OrganizationID");

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_Users_UserID",
                table: "Visits",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visits_Users_UserID",
                table: "Visits");

            migrationBuilder.DropTable(
                name: "DentalCharts");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teeths");

            migrationBuilder.DropIndex(
                name: "IX_Visits_UserID",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Visits");
        }
    }
}
