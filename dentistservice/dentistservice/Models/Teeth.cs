﻿using dentistservice.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace dentistservice.Models
{
    public class Teeth : IEntity<int>
    {
        public int ID { get; set; }
        public int JawType { get; set; }
        public int JawSide { get; set; }
        public int ToothNo { get; set; }
    }
}
