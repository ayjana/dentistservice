﻿using dentistservice.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistservice.Models
{
    public class User : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Login { get; set; }

        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }

        [Required]
        [MaxLength(500)]
        public string Password { get; set; }

        [Required]
        public bool IsBlocked { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationID { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
