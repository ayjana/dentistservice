﻿using dentistservice.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistservice.Models
{
    public class Patient : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [MaxLength(100)]
        public string SurName { get; set; }
        public string Patronymic { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public bool Gender { get; set; }
        public DateTime CreatedAt { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationID { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
