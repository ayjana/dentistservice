﻿using dentistservice.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistservice.Models
{
    public class DentalChart : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Info { get; set; }

        [MaxLength(1000)]
        public string Comment { get; set; }

        [ForeignKey("Teeth")]
        public int TeethID { get; set; }
        public virtual Teeth Teeth { get; set; }

        [ForeignKey("Visit")]
        public int VisitID { get; set; }
        public virtual Visit Visit { get; set; }
    }
}
