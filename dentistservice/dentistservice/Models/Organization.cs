﻿using dentistservice.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace dentistservice.Models
{
    public class Organization : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Address { get; set; }

        [Required]
        [MaxLength(100)]
        public string Supervisor { get; set; }
    }
}
