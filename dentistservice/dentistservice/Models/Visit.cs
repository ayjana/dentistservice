﻿using dentistservice.Enums;
using dentistservice.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dentistservice.Models
{
    public class Visit : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        public VisitType VisitType { get; set; }

        [Required]
        public DateTime VisitDate { get; set; }

        [Required]
        public VisitStatus VisitStatus { get; set; }
        public decimal? Price { get; set; }

        [ForeignKey("Patient")]
        public int PatientID { get; set; }
        public virtual Patient Patient { get; set; }

        [ForeignKey("User")]
        public int? UserID { get; set; }
        public virtual User User { get; set; }

    }
}
