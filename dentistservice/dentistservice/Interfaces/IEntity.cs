﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dentistservice.Interfaces
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
