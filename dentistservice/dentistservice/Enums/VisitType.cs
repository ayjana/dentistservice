﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dentistservice.Enums
{
    public enum VisitType
    {
        FirstVisit = 1,
        SecondVisit = 2
    }
}
