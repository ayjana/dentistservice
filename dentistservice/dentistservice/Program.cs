﻿using dentistservice.Enums;
using dentistservice.Models;
using System;

namespace dentistservice
{
    class Program
    {
        static void Main(string[] args)
        {
            Seed();

        }

        static void Seed()
        {
            using (var db = new ApplicationDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    var organization = new Organization
                    {
                        Name = "Dentist Service",
                        Supervisor = "Dentist",
                        Address = "A"
                    };

                    db.Organizations.Add(organization);
                    db.SaveChanges();

                    var patient = new Patient
                    {
                        Name = "A",
                        SurName = "K",
                        CreatedAt = DateTime.Now,
                        Gender = false,
                        DateOfBirth = new DateTime(2002, 07, 29),
                        OrganizationID = organization.ID
                    };

                    db.Patients.Add(patient);
                    db.SaveChanges();

                    var user = new User
                    {
                        FullName = "AK",
                        Login = "AK",
                        CreatedAt = DateTime.Now,
                        OrganizationID = organization.ID,
                        Password = "AK"
                    };

                    db.Users.Add(user);
                    db.SaveChanges();

                    var visit = new Visit
                    {
                        PatientID = patient.ID,
                        UserID = user.ID,
                        VisitDate = DateTime.Now.AddDays(10),
                        VisitStatus = VisitStatus.Created,
                        VisitType = VisitType.FirstVisit
                    };

                    db.Visits.Add(visit);
                    db.SaveChanges();

                    transaction.Commit();
                }
            }
        }
    }
}
